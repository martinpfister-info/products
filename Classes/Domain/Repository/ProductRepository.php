<?php
namespace Pfister\Products\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Martin Pfister <mail@martinpfister.info>, martinpfister.info
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Persistence\Repository;

class ProductRepository extends Repository
{
    /**
     * @param array $categories
     * @return array
     */
    function findByCategories(array $categories)
    {
        $query = $this->createQuery();
        $result = [];
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->matching($query->in('categories', $categories));

        /** @var \Pfister\Products\Domain\Model\Product $product */
        foreach ($query->execute()->toArray() as $product) {
            $result[$product->getCategory()->getUid()]['products'][] = $product;
            $result[$product->getCategory()->getUid()]['category'] = $product->getCategory();
        }

        return $result;
    }
}
